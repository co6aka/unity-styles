﻿using UnityEngine;
using System.Collections;

public class Piggy : MonoBehaviour
{

    public float walkSpeed = 10f; 

	Rigidbody2D body;
    private float halfHeight;  // how far down to raycast

    void Start ()
	{
		body = GetComponent<Rigidbody2D>();
        halfHeight = GetComponent<SpriteRenderer>().bounds.size.y / 2 + 0.1f;
    }

	// Update is called once per frame
	void Update ()
	{
        // positive or negative (right or left)
        // we let our piggy walk in the air, but not jump..
        float walk = Input.GetAxis("Horizontal");

        // piggy always moves in air or on ground, regardless if it's in air or not
        body.velocity = new Vector2(walkSpeed * walk, body.velocity.y); // positive walks right, negative left
	}

}
