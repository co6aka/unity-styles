﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class ItemManager : MonoBehaviour {

    public Text coinsLabel;
    public Text selectedLabel;

    [HideInInspector]
    public string selected = null;  // set by button clicks
    public static int Balance = 250;
    int COST = 35;  // for simplicty, allitems have the same cost

    // prefab object names
    private string[] defenders = {
        "Cactus", "Gator", "Iguana", "MoneyTree", "Monkey", "PoisonPlant", "Rum", "Scorpion", "Turtle"
    };
    private Dictionary<string, GameObject> items = new Dictionary<string, GameObject>();

    void Start() {
        // load defender prefabs
        foreach (string name in defenders) {
            GameObject def = (GameObject)Resources.Load("Prefabs/defenders/" + name);
            items[name] = def;
        }
    }

    public void Select(string name)
    {
        selected = name;
        selectedLabel.text = "Selected " + name;
    }

    public GameObject Plant()
    {
        if (selected == null || selected == "")       // nothing selected yet (at game start)
            return null;

        if (Balance <= 0)
            return null;

        GameObject template = items[selected];
        GameObject item = Instantiate(template);

        Balance -= COST;
        return item;
    }

    public void GameOver()
    {
        selectedLabel.text = "Game Over :(";
        Balance = -1000;     // so we can't plant defenders
    }
}
