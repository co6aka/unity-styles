﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// hand fighting - applies to both attackers and defenders
public class Grunt : MonoBehaviour
{
    private Character player;
    
    private Dictionary<string, GameObject> enemySwarm
        = new Dictionary<string, GameObject>();      // since onTriggerExit() is not fired when enemy is destroyed :(
    private int closeEnemyCount;            // append index to each enemy's name as they're not unique
    private SpriteRenderer sr;

    void Start()
    {
        player = GetComponent<Character>();
        sr = GetComponent<SpriteRenderer>();
    }

    // Check if we're hand-fighting anyone   
    void FixedUpdate()
    {
        // count close enemies: if any, stay engaged
        // and decrease health
        // otherwise, notify Character we're free 
        
    }

    // ran into something: tile, friendly, projectile or enemy
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Rum")     
        {
            // tell Character to get drunk
            return;
        }

        if (other.tag == "Poison")
        {
            // tell Character to die
            return;
        }

        //print("grunt collision, my tag: " + tag + " other tag: " + other.tag);
        if (CloseContact(tag, other.tag))
        {             // close combat with actual enemy
            string key = other.name + closeEnemyCount++;
            enemySwarm.Add(key, other.gameObject);

            //player.Engaged = true;      // will set the proper animation, etc
        }
    }

    private bool CloseContact(string tag1, string tag2)
    {
        if (tag1 == "Attacker" && tag2 == "Defender")
            return true;
        if (tag2 == "Attacker" && tag1 == "Defender")
            return true;
        return false;
    }
}
