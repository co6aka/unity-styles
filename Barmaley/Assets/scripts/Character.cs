using System.Collections.Generic;
using UnityEngine;

// an attacker or defender - anyone with any business on the battlefield
public class Character : MonoBehaviour {

    [SerializeField]
    [Range(120, 1200)]
    private int health = 600;  // add getter / setter -> should die when out of health

    public float expireTime = 2.5f;        // length of death animation, before object is destroyed

    public int cost;                    // applies mostly to Defending items

    // add getters/setters
    private bool engaged;      // if engaged, stop & start hand-fight, othewise keep moving    
    private bool dead;          
    private bool drunk;
  

	void Start () {
        AdjustStartPos();
    }


    /* Some of the defenders, such as gator and scorpion are short, 
     *  so we place them towards the bottom of their tile */
    private void AdjustStartPos()
    {
        if (name.StartsWith("Scorpion") || name.StartsWith("Gator"))      // they're all Clones, so are their names
            transform.position = new Vector2(transform.position.x, transform.position.y - 0.25f);

        if (name.StartsWith("Turtle") || name.StartsWith("PoisonPlant"))
            transform.position = new Vector2(transform.position.x, transform.position.y - 0.15f);
    }

    // engaged in hand-fight: stop and set up proper animation
    private void StartFighting()
    {        
    }

    // not hand-fighting any more..
    private void ResumeMovement()
    {
    }

    public void Shoot()     // called by action animation when it's ok to launch projectile
    {

    }

    private void Die() {

        if (dead == true)
            return;

        GetComponent<SpriteRenderer>().color = Color.white;  // restore if poisoned or other
        dead = true;
        // actiate "killed" animation
        // Destroy object after a short time, so that death anination fully plays out		
        Destroy(gameObject, expireTime);          // let the death animation play out
	}

}
